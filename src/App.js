import React, { Component } from 'react';
import './App.css';
import Plateau from "./Plateau"
import Header from './Header.js'

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>

              <Plateau/>
                <hr/>

                <footer>
                    <p>Copyright &copy; Web2 L3 UT2J</p>

                </footer>
            </div>
        );
    }
}

export default App;
