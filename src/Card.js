import React, { Component } from 'react'
import './App.css';

class Card extends Component{
    constructor(props){
        super(props);


    }

    render() {

        return (
            <div className="Card" key={this.props.key}>
                <div className="card bg-dark text-white border-warning border-3">
                    <img className="card-img"
                         src={"https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" + this.props.src}/>
                    <p className="card-text">{this.props.name}</p>
                </div>
            </div>

        );
    }
}

export default Card;
