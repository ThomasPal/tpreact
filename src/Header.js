import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <div className="Header">
                <header className="bg-dark">
                    <nav className="navbar bg-dark">
                        <span className="navbar-brand text-secondary">LOL MEMO</span>
                    </nav>
                </header>
            </div>
        );
    }
}

export default Header;
