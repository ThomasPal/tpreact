import React, { Component } from 'react';
import Card from "./Card"
import axios from 'axios'

class Plateau extends Component {
    constructor(props){
        super(props);

        this.state = {
            champions:null,
            error:null,
            chosen:[]
        };
    }
    componentDidMount(){
        axios.get("champions.json")
            .then(res=>
            {
                this.setState({champions:res.data});
                this.takeThreeChamp();
            })
            .catch(error=>this.setState(error));

    }
    takeThreeChamp(){
        let nbChamp = 12;
        let champions = [];
        for(let champion in this.state.champions.data){
            champions.push(champion);
        }

        let chosenChamps = [];
        for(let i=0;i<nbChamp;i++){
            let champion = champions[Math.floor(Math.random()*champions.length)];
            if(chosenChamps.indexOf(champion)===-1){
                chosenChamps.push(champion);
            }else{
                i--;
            }
        }

        let chosen = [];
        for(let i = 0;i< chosenChamps.length;i++){
            chosen.push({name:chosenChamps[i],src:chosenChamps[i]+"_0.jpg"});
            chosen.push({name:chosenChamps[i],src:chosenChamps[i]+"_1.jpg"});
        }
        console.log(chosen.length);

        chosen = this.shuffle(chosen);

        this.setState({chosen:chosen});
    }
    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    render() {

        const cards = this.state.chosen.map((card)=>{
            return <Card key={card.name} name={card.name} src={card.src}/>
        });
            return (
                <div className="">
                    <section className="card-columns ">
                        {cards}
                    </section>
                </div>
            );

    }
}

export default Plateau;
